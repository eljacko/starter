package com.eljacko.starter.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
@SuppressWarnings({"checkstyle:designforextension", "checkstyle:HideUtilityClassConstructor"})
public class WebApiApplication {


    public WebApiApplication() {
        super();
    }

    public static void main(final String[] args) {
        SpringApplication.run(WebApiApplication.class, args);
     }
}
