package com.eljacko.starter.webapi.constant;

public final class WebApiPaths {

    public static final String HOME = "/";


    private WebApiPaths() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
