package com.eljacko.starter.webapi.exception;

import com.eljacko.starter.webapi.dto.FieldError;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class InvalidParametersException extends RuntimeException {

    private static final long serialVersionUID = 7979151036772999042L;
    private List<FieldError> fieldErrors;

    public InvalidParametersException(final List<FieldError> fieldErrors) {
        super();
        this.fieldErrors = fieldErrors;
    }

}
