package com.eljacko.starter.datamodel.utils;

import java.util.regex.Pattern;

public final class StringUtil {

    private static final String NUMBER_PATTERN = "^[0-9]*\\.?[0-9]*?$";


    public static String parseString(final String term) {
        return "%" + term.toLowerCase().trim() + "%";
    }

    public static boolean isBigDecimalString(final String s) {
        return Pattern.compile(NUMBER_PATTERN).matcher(s).matches();

    }

    private StringUtil() {
        throw new UnsupportedOperationException(
                "This is a utility class and cannot be instantiated");
    }

}
