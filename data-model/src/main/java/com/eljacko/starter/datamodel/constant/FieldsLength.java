package com.eljacko.starter.datamodel.constant;

public final class FieldsLength {

    public static final int FIRST_NAME = 64;

    private FieldsLength() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
