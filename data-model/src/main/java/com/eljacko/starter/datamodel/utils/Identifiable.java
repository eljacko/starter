package com.eljacko.starter.datamodel.utils;

import java.io.Serializable;

public interface Identifiable<T extends Serializable> {
    T getId();
}
